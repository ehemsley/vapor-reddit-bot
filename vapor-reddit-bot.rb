    require 'redditkit'


    if ARGV[0].nil? or ARGV[1].nil?
      puts 'Please specify username and password.'
      exit
    elsif ARGV[2].nil?
      puts 'Please specify audio directory.'
      exit
    end
    username = ARGV[0]
    password = ARGV[1]
    audio_directory = ARGV[2]
    playlist_script_directory = ARGV[3]


    RedditKit.sign_in username, password
  
    RedditKit.user_agent = 'vapor-fm-butler v0.2 created by /u/that-cosmonaut'


      most_recent_comment_id = nil
      if File.exist?('./most_recent_comment_id') and !File.zero?('./most_recent_comment_id')
        File.open('./most_recent_comment_id') { |f| most_recent_comment_id = f.readline.strip }
      else
        File.open('./most_recent_comment_id', 'w') {}
      end

    comments = RedditKit.user_content('that-cosmonaut', category: :comments, before: most_recent_comment_id).results


    comments.each do |comment|
        if comment.text =~ /[bB]utler/
          if comment.text =~ /add.+to.+vapor fm/
                confirmation_word = ['Certainly', 'Gladly', 'Of course', 'Understood', 'Very well'].sample

              RedditKit.submit_comment(comment, confirmation_word + '.')

              link = RedditKit.link(comment.link_id)
              soundscrape_command = `which soundscrape`
              Dir.chdir(audio_directory) do
                if link.url =~ /bandcamp/
                  system "#{soundscrape_command} #{link.url} -b -f"
                    Dir.chdir(playlist_script_directory) do
                      system "generate_playlist.sh"
                    end

                elsif link.url =~ /soundcloud/
                  system "#{soundscrape_command} #{link.url} -f"
                    Dir.chdir(playlist_script_directory) do
                      system "generate_playlist.sh"
                    end

                end
              end

          end
        end

    end
      if comments.any?
        File.delete('./most_recent_comment_id')
        File.open('./most_recent_comment_id', 'w') { |f| f.write(comments.first.full_name) }
      end



    STDOUT.write("#{Time.now}: Task completed")


